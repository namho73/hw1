﻿using System;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.App;
using Android.Widget;
using Android.OS;

namespace HW1
{
    [Activity(Label = "HW1", MainLauncher = true)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
        }
    }
}

